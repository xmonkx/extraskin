.PHONY: all watch clean

SASS := sass
SASS_FLAGS := --style=expanded --sourcemap=none

ROOT_SASS := src/godville.sass
ROOT_CSS := $(ROOT_SASS:.sass=.css)

all: $(ROOT_CSS)

watch:
	$(SASS) $(SASS_FLAGS) --watch $(ROOT_SASS)

clean:
	$(RM) $(ROOT_CSS)

%.css: %.sass
	$(SASS) $(SASS_FLAGS) $^ $@
